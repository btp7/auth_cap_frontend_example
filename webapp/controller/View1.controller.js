sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("frontend.controller.View1", {
            onInit: function () {

            },
            onPress1: function(){
                var sUrl = "vcfservice/catalog/Books"
                var sUrl = this.getOwnerComponent().getManifestObject().resolveUri(sUrl);

            $.ajax({
                method: "GET",
                url: sUrl,
                Content: "application/json",
                dataType: "json",
                success: this.successREST.bind(this),
                error: [this.errorREST, this]
            });
        },
        onPress2: function(){
            var sUrl = "vcfservice/catalog-service2/Books"
            var sUrl = this.getOwnerComponent().getManifestObject().resolveUri(sUrl);

            $.ajax({
                method: "GET",
                url: sUrl,
                Content: "application/json",
                dataType: "json",
                success: this.successREST.bind(this),
                error: [this.errorREST, this]
            });
        },
        successREST: function (data, status, response) {},
        errorREST: function (error) {},

        });
    });
